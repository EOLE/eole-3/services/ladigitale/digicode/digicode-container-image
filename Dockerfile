FROM hub.eole.education/proxyhub/bitnami/git:latest as BUILD
COPY app-version /tmp/
RUN cd /tmp && \
    git clone https://gitlab.mim-libre.fr/EOLE/eole-3/services/ladigitale/digicode/digicode-sources.git && \
    cd digicode-sources && \
    git checkout $(cat ../app-version) && \
    cp -a /tmp/digicode-sources /tmp/src
FROM hub.eole.education/proxyhub/library/nginx:alpine
COPY --from=BUILD /tmp/src/ /usr/share/nginx/html
COPY server_tokens.conf /etc/nginx/conf.d/
VOLUME /usr/share/nginx/html
VOLUME /etc/nginx
EXPOSE 80

